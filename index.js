/**
 * Bài 1: Tính tiền lương nhân viên với 1 ngày lương là 100.000
 */
function caculateSalary() {
  let oneDaySalary = document.getElementById("oneDaySalary").value * 1;
  let day = document.getElementById("day").value * 1;

  let totalSalary = oneDaySalary * day;

  document.getElementById("salary").innerText = totalSalary;
}
/**
 * Bài 2: Tính giá trị trung bình của 5 số thực và xuất ra màn hình
 */
function caculateAvergrateNum() {
  var num1 = +document.getElementById("num1").value;
  var num2 = +document.getElementById("num2").value;
  var num3 = +document.getElementById("num3").value;
  var num4 = +document.getElementById("num4").value;
  var num5 = +document.getElementById("num5").value;

  var avergrateNum = (num1 + num2 + num3 + num4 + num5) / 5;

  document.getElementById("avergrateNum").innerText = avergrateNum;
}
/**
 * Bài 3: Quy đổi sang từ USD sang VND với giá 1 USD = 23.500 VND
 */
function changeVnd() {
  usd = +document.getElementById("USD").value;
  document.getElementById("VND").innerText = usd * 23500;
}
/**Bài 4: Tính diện tích và chu vi hình chữ nhật khi nhập vào chiều dài và chiều rộng
 */
function caculate() {
  var longOfRec = document.getElementById("long").value * 1;
  var widthOfRec = document.getElementById("width").value * 1;
  var area = longOfRec * widthOfRec;
  var perimeter = (longOfRec + widthOfRec) * 2;
  document.getElementById(
    "result"
  ).innerText = `Chu vi là: ${perimeter} m và diện tích là: ${area} m`;
}
/**Bài 5: Tính tổng 2 ký số
 */
function sum() {
  var number = document.getElementById("number").value * 1;
  var unit = number % 10;
  var ten = Math.floor(number / 10) % 10;
  var total = unit + ten;

  document.getElementById("sum").innerText = total;
}
